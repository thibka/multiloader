import "@babel/polyfill";

import './styles/main.scss';

import * as THREE from 'three';
import PostProcessor from './js/PostProcessor.js';
import Files from './js/MultiLoader/Files.js';
import MultiLoader from './js/MultiLoader/MultiLoader.js';

var clock = new THREE.Clock();
var canvas = document.querySelector("#canvas");

var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera(50, canvas.width / canvas.height, 1, 100);
scene.add(camera);
camera.position.z = 10;
camera.lookAt(new THREE.Vector3());
var callbacks = [];

function beforeRender(cb) {
	callbacks.push(cb);
}

function setPostProcessor() {
    PostProcessor.init(scene, camera);
    //PostProcessor.add('pixelation');
}

function resize() {
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;

    PostProcessor.composer.setSize(canvas.width, canvas.height);

    camera.aspect = canvas.width / canvas.height;
    camera.updateProjectionMatrix();
}

function render() {
	requestAnimationFrame(render);
	callbacks.forEach(function (callback) { callback() });
	var delta = clock.getDelta();
    PostProcessor.composer.render(delta);
}

/* */

MultiLoader.load({
    files: Files,
    onLoading: function(percent) {
        // 'percent' returns a value between 0 and 1 
    },
    onFinish: init
});

function init() {
    setPostProcessor();
    window.addEventListener('resize', resize);
    resize();
    createScene();
    render();
}

function createScene(){
    var cube = new THREE.Mesh(
        new THREE.BoxBufferGeometry(2, 2, 2),
        new THREE.MeshStandardMaterial({
            map: Files.metal.texture,
            side: THREE.DoubleSide,
            metalness: .8,
            roughness: .5
        })
    );
    scene.add(cube);

    var landscape = Files.landscape.scene;
    landscape.position.z = 10;
    landscape.position.y = -10;
    scene.add(landscape);

    var chair = Files.chair.object;
    chair.scale.set(.1,.1,.1);
    scene.add(chair);
    
    var light = new THREE.PointLight(0xffffff, 1);
    light.position.set(10, 10, 30);
    scene.add(light);
    scene.add(new THREE.AmbientLight(0xacacac));
    
    beforeRender(function () {
        cube.rotation.y += 0.01
    });
}





