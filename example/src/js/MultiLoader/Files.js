const Files = {
    metal: {type: 'texture', path: require('../../img/metal.jpg')},
    landscape: { type: 'gltf', path: '../../gltf/scene.gltf' }, // Pas de require pour les GLTF !
    chair: {type: 'obj', path: require('../../models/chair.obj')}
};

export default Files;