/* eslint-disable */

const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

let config = {
	mode: 'development',
	entry: './src/index.js',
	output: {
		path: path.resolve(__dirname, './public'),
		filename: './bundle.js'
	},
	watch: true,
	module: {
		rules: [
			{
                test: /\.(png|jpe?g|gif|mp3|woff|woff2|eot|ttf|otf|obj)$/,
                exclude: /gltf/,
				use: [
					'file-loader'
				]
			},
			{
				test: /\.(svg)$/i,
				use: [
				  'file-loader',
				  {
					loader: 'image-webpack-loader',
					options: {
					  bypassOnDebug: true,
					},
				  },
				],
			},
			{
				test: /\.s?css$/,
				use: [
					MiniCssExtractPlugin.loader,
					"css-loader",
					"sass-loader"
				]
			},
			{
				type: 'javascript/auto',
				test: /\.json$/,
				loader: 'file-loader',
				options: {
					name: '[name].[ext]'
				}
            },
            {
                test: /\.m?js$/,
                //exclude: /(node_modules|bower_components)/,
                use: {
                  loader: 'babel-loader',
                  options: {
                    presets: ['@babel/preset-env']
                  }
                }
            }
		]
    },
    devServer: {
        contentBase: path.join(__dirname, 'public'),
        watchContentBase: true
    },
	plugins: [
		new HtmlWebpackPlugin({
			hash: true,
			minify: false,
			filename: './index.html',
			template: 'url-replace-loader!./src/index.html'
		}),
		new MiniCssExtractPlugin({
			filename: "[name].css",
			chunkFilename: "[id].css"
		}),
		new CleanWebpackPlugin('app'),
		new CopyWebpackPlugin([
            //{from: 'src/img/favicon.ico', to: 'favicon.ico'},
            {from: 'src/models/gltf', to: 'gltf'},
        ])
	],
	
};

module.exports = config;